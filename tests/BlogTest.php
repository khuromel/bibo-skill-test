<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogTest extends TestCase
{
    use WithoutMiddleware;

    public function testGetBlog()
    {
       $response = $this->call('GET', '/api/get_all_blog', [ ]);
       $this->assertEquals(200, $response->status());
    }

    public function testCreate()
    {
       $response = $this->call('POST', '/api/create', [ 'id' => 1,
                                                        'user_id' => 1,
                                                        'title' => 'Test Blog',
                                                        'slug' => 'Test-blog',
                                                        'content' => 'Sample blog content',
                                                        'created_at' => '2016-11-02 12:50:55',
                                                        'updated_at' => '2016-11-02 12:50:55'
                                                        ]);
       $this->assertEquals(200, $response->status());
    }

    public function testEdit()
    {
       $response = $this->call('POST', '/api/edit/1', ['blog_id' => 1,
                                                        'user_id' => 1,
                                                        'title' => 'Test Blog',
                                                        'slug' => 'Test-blog',
                                                        'content' => 'Sample blog content',
                                                        'created_at' => '2016-11-02 12:50:55',
                                                        'updated_at' => '2016-11-02 12:50:55'
                                                        ]);
       $this->assertEquals(200, $response->status());
    }

    public function testDelete()
    {
       $response = $this->call('POST', '/api/delete/1', []);
       $this->assertEquals(200, $response->status());
    }
}
