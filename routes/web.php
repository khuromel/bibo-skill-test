<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

//Home
Route::get('/', 'HomeController@index');
Route::get('/blog/create', 'BlogsController@create');
Route::get('/blog/edit/{blog_id}', 'BlogsController@edit');

Route::get('/api/get_all_blog', 'BlogsController@getAllBlog');
Route::post('/api/create', 'BlogsController@store');
Route::post('/api/edit/{blog_id}', 'BlogsController@update');
Route::post('/api/delete/{blog_id}', 'BlogsController@delete');

Route::get('/blog/{slug}', 'BlogsController@show');
