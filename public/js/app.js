var app = angular.module('myApp', []);
app.controller('blogFeed', function($scope, $http) {

    $http({
        url: '/api/get_all_blog',
        method: "GET",
    })
    .then(function(response) {
        $scope.blogData = response.data.body.article;
    },
    function(response) {
      var errors = response.data;
      var errorsHtml = ''
      $.each( errors, function( key, value )
      {
        errorsHtml +=  value + '\r\n';
      });
      sweetAlert("Oops...", errorsHtml, "error");
    });

    $scope.DeletePost = function(blog_id){
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this post!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        $http({
            url: '/api/delete/'+blog_id,
            method: "POST",
        })
        .then(function(response) {
            // success
            swal({title:"Success!", text:"You have deleted your article.", type:"success"}, function(){ window.location.href='/' });
        },
        function(response) {
          sweetAlert("Oops...", response.data, "error");
        });
      });
    };

});


//Create
app.controller('blogCreate', function($scope, $http) {


    $scope.addPost = function(){
      $http({
          url: '/api/create',
          method: "POST",
          data: {  title: $scope.title, slug: $scope.slug, content: $scope.content}
      })
      .then(function(response) {
          // success
          swal({title:"Success!", text:"You have created your article.", type:"success"}, function(){ window.location.href='/' });
      },
      function(response) {
        var errors = response.data;
        var errorsHtml = ''
        $.each( errors, function( key, value )
        {
          errorsHtml +=  value + '\r\n';
        });
        sweetAlert("Oops...", errorsHtml, "error");
      });
    };

});


//Create
app.controller('blogEdit', function($scope, $http) {

    $scope.UpdatePost = function(){
      var blog_id = $("#blog_id").val()
      var title = $("#title").val()
      var slug = $("#slug").val()
      var content = $("#content").val()
      $http({
          url: '/api/edit/'+blog_id,
          method: "POST",
          data: {  title: title, content: content}
      })
      .then(function(response) {
          // success
          swal({title:"Success!", text:"You have updated your article.", type:"success"}, function(){ window.location.href='/' });
      },
      function(response) {
        var errors = response.data;
        var errorsHtml = ''
        $.each( errors, function( key, value )
        {
          errorsHtml +=  value + '\r\n';
        });
        sweetAlert("Oops...", errorsHtml, "error");
      });
    };

});
