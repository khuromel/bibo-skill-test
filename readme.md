
## About This Sample Blog

This is a simple blog powered by Laravel and AngularJS

- Laravel  https://laravel.com
Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

- Angular https://angularjs.org
HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop.

##Pre-requisite

mysql
http://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.16-osx10.11-x86_64.tar.gz

apache
http://mirror.rise.ph/apache//httpd/httpd-2.4.23.tar.gz

php 5.6
http://hk1.php.net/get/php-5.6.27.tar.gz/from/this/mirror

## Installation
- Copy all source files into the virtual server user scp or rsync
- Run sudo sh /installation/install.sh

This will install all the LAMP stack from source.

- Additional Config
CONFIGURATION OPTIONS
You may want to change the document root – replace the line

DocumentRoot "/usr/local/apache2/htdocs"
With

DocumentRoot "/var/www/htdocs" # Or whatever folder you want to set as the document root.
Also change the line

<Directory "/usr/local/apache2/htdocs">
to

<Directory "/var/www/htdocs">

- Update local hosts file
Mac: run this command: sudo nano /private/etc/hosts
Windows: open c:/Windows/System32/drivers/etc/hosts

-Note: for Laravel it is recommended to install the environment via the use of Homestead
https://laravel.com/docs/5.3/homestead

##Laravel installation

- Install php composer
Follow instruction here: https://getcomposer.org/

- Run this to install laravel
composer global require "laravel/installer"

-Create new application
laravel new Bibo

## Application Infrastructure
- Blade
  Templating engine for laravel
- Laravel Auth
  Authentication library to create the login/register/forgetpassword/change_email functions.
- Eloquent / Database Builder
  This is used to fetch data from the Database
- AngularJS
  This is used on some pages to fetch and send data to API endpoints.
- Request StoreBlog
  This is used for server side validation
- Sweet Alert for alert actions

##Database
- Run php artisan migrate, this will run the laravel migration tool with the pre-defined schema

## Unit Testing
- I create a simple test to test the API endpoints and are found in /test/BlogTest.php
To run this, go to the root url and run phpunit.
