<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
@extends('common.head')
</head>
<body>
        @extends('common.header')

        @yield('content')

        @extends('common.footer')
</body>
</html>
