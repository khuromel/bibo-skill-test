@extends('layouts.app')

@section('content')


<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Blog about the Universe</h1>
                    <hr class="small">
                    <span class="subheading">A blog inspired by the cosmos.</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->

<div class="container" ng-controller="blogFeed">
    @if (Auth::guest())
      <h3>
          <center>Please login <a href="/login">here</a> to view posts.</center>
      </h3>
    @else
    <div class="row">

          <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <div class="post-preview" ng-repeat="blog in blogData">
                <a href="/blog/@{{ blog.slug}}">
                    <h2 class="post-title">
                        @{{ blog.title}}
                    </h2>
                    <h3 class="post-subtitle">
                        @{{ blog.content | limitTo:100}} ...
                    </h3>
                </a>
                <p class="post-meta">Posted by <a href="#">@{{ blog.user_id}}</a> on @{{ blog.created_at}}</p>

                    <a href="/blog/edit/@{{ blog.id }}"><i class="fa fa-pencil"></i> Edit </a> &nbsp;&nbsp;&nbsp;
                    <a href="" ng-click="DeletePost(blog.id)"><i class="fa fa-trash"></i> Delete </a>
            </div>
            <hr>
          </div>

            <!-- Pager -->
            <!-- <ul class="pager">
                <li class="next">
                    <a href="#">Older Posts &rarr;</a>
                </li>
            </ul> -->
    </div>
    @endif
</div>

<hr>

@endsection
