@extends('layouts.app')

@section('content')
<header class="intro-header" style="background-image: url('/img/post-bg.jpg')">
    <div class="container" ng-controller="blogEdit">
        <div class="row">
            <div style="margin-top:150px"></div>
            <div class="panel panel-default">
                <div class="panel-heading">Edit a Post</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" novalidate >
                      <input type="hidden" id="blog_id" name="blog_id" value="{{ $blog[0]->id }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="title" class="form-control" name="title" value="{{ $blog[0]->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                            <label for="slug" class="col-md-4 control-label">Slug</label>

                            <div class="col-md-6">
                                <input id="slug"  type="slug" class="form-control" name="slug" value="{{ $blog[0]->slug }}" required autofocus disabled="true">

                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Content</label>

                            <div class="col-md-6">
                                <textarea id="content" type="content" class="form-control" name="content" required autofocus rows="10" cols="200">
                                  {{ $blog[0]->content }}
                                </textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" ng-click="UpdatePost()">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>


@endsection
