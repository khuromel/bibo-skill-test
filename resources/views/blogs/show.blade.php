@extends('layouts.app')

@section('content')

<header class="intro-header" style="background-image: url('/img/post-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1>{{$article[0]->title}}</h1>
                    <span class="meta">Posted by <a href="#">{{$article[0]->user_id}}</a> on {{$article[0]->created_at}}</span>
                </div>
            </div>
        </div>
    </div>
</header>

<article>
  <div class="container">
      <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
              {{$article[0]->content}}
          </div>
      </div>
  </div>
</article>

@endsection
