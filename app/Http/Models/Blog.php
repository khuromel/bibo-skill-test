<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Blog extends Eloquent
{

  protected $fillable = [
    'title',
    'slug',
    'content',
    'user_id'
];
}
