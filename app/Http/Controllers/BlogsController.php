<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Blog;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BlogsController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function getAllBlog()
    {
      $blogs = DB::table('blogs')
      ->orderBy('created_at', 'desc')
      ->get();
      return response()->json([
          'status' => 200,
          'message' => 'success',
          'body' => ['article' => $blogs]
      ]);
    }

    public function show($slug)
    {
      $article = DB::table('blogs')->where('slug', $slug)
      ->get();
      return view('blogs.show',[
          'article' => $article,
      ]);
    }

    /**
     * Show the form to create a new Blog.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
      return view('blogs.create');
    }

    /**
     * Store a new Blog.
     *
     * @param Requests\SaveBlog $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Requests\StoreBlog $request)
    {
        $request->merge([
            'user_id'    => Auth::user()->id,
        ]);
        $Blog = Blog::create($request->all());
    }

    /**
     * Show the form to edit an existing Blog.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $blog = DB::table('blogs')->where('id', $id)
        ->get();
        return view('blogs.edit', [
            'blog' => $blog,
        ]);
    }

    /**
     * Update a new Blog.
     *
     * @param Requests\SaveBlog $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Requests\StoreBlog $request)
    {

        Blog::where('id', $request->blog_id)
             ->findOrFail($request->blog_id)
             ->fill($request->all())
             ->save();
    }

    /**
     * Delete an existing Blog.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $Blog = Blog::where('id', $request->blog_id)
            ->findOrFail($request->blog_id);

        $Blog->delete();
    }


}
