Apache
 cd /usr/src/httpd
 sudo wget http://mirror.rise.ph/apache//httpd/httpd-2.4.23.tar.gz
 gunzip < httpd-2.4.23.tar.gz | tar -xvf -
 cd httpd-2.4.23.tar.gz
 sudo ./configure --prefix=/usr/local/apache2
 sudo make
 sudo make install
sudo ./configure --prefix=/usr/local/apache2 --enable-mime-magic --enable-expires \
--enable-headers --enable-ssl --enable-http --enable-info --enable-dir \
--enable-rewrite --enable-so

Mysql
 cd /usr/src/mysql
 sudo wget http://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.16.tar.gz
 gunzip < mysql-5.7.16.tar.gz | tar -xvf -
 cd mysql-5.7.16.tar.gz
 groupadd mysql
 useradd -g mysql mysql
 ./configure --prefix=/usr/local/mysql [add the necessary extra options here]
 sudo make
 sudo make install
 cp support-files/my-medium.cnf /etc/my.cnf
 cd /usr/local/mysql
 bin/mysql_install_db --user=mysql
 sudo chown -R root  .
 sudo chown -R mysql lib
 sudo chgrp -R mysql .
 sudo bin/mysqld_safe --user=mysql &

PHP
 cd /usr/src/php5
 wget -O php-5.6.27.tar.gz http://de1.php.net/get/php-5.6.27.tar.gz/from/this/mirror
 gunzip < php-5.6.27.tar.gz | tar -xvf -
 cd php-5.6.27.tar.gz
 sudo ./configure \
--prefix=/usr/local/php5 \
--with-apxs2=/usr/local/apache2/bin/apxs \
--with-mysql=shared,/usr/local/mysql [add your options here]
./configure --prefix=/usr/local/php5 --with-apxs2=/usr/local/apache2/bin/apxs \
--with-mysql=shared,/usr/local/mysql --with-zlib --with-gettext --with-gdbm --with-sqlite
 ./configure \
 --prefix=/usr/local/php5 \
 --with-apxs2=/usr/local/apache2/bin/apxs \
 --with-mysql=shared,/usr/local/mysql [add your options here]
 sudo make
 sudomake install
 cp php.ini-production /usr/local/php5/lib
 cd /usr/local/php5/lib
 sudo mv php.ini-production php.ini-production
